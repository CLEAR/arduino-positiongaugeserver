/*
 * Server for transmitting multiple Mitutoyo position gauge measurements over ethernet.
 * Access via HTTP on TCP port 80, or very simple text protocol (telnet-like), one port per device.
 * 
 * Example for accessing over text interface (best for machine clients):
 *  nc 192.168.1.31 21
 * or
 *  nc 192.168.1.31 22
 * Netcat (nc) is the best client: it is a clean TCP client,
 * which makes no attempts to negotiate anything.
 *  
 * Kyrre Ness Sjobak, 15 Feb 2021
 * University of Oslo / CERN
 * 
 * Inspired by:
 * https://github.com/Roger-random/mitutoyo/blob/master/src/main.cpp
 * Ethernet library examples, especially
 * - WebServer
 * - DhcpAddressPrinter
 * - EthernetServer::accept() example
 */

#include <Arduino.h>

#include <SPI.h>
#include <Ethernet.h>

//This file holds all the configurations
// to be modified for deployment
#include "PositionGaugeServer_config.h"

// ***** GLOBAL VARIABLES ***********************

unsigned long prevUpdateTime = 0;

float measData[numCh];
bool  measDataFresh[numCh];
bool  measDataIsMm[numCh];
bool  measDataIsValid[numCh];

EthernetServer srvWEB(80);

uint16_t srvTXT_port[numCh] = {21,22};
EthernetServer srvTXT[numCh] = {
  EthernetServer(srvTXT_port[0]),
  EthernetServer(srvTXT_port[1])
};

EthernetClient clients[MAX_SOCK_NUM];
uint8_t        clients_ch[MAX_SOCK_NUM]; //which channel does a particular client belong to?

unsigned long srvTXT_prevUpdateTime[numCh];

// ***** CODE ************************************

void setup() {

  //Serial setup, for debugging
  Serial.begin(9600);

  // wait for serial port to connect. Needed for native USB port only
  int serialCounter=0;
  while (!Serial) {
    //However only wait for 1 second before giving up,
    // so that it can work also when not connected to USB
    delay(100);
    serialCounter++;
    if (serialCounter > 10) break;
  }
  Serial.println();
  Serial.println("PositionGaugeServer initializing...");

  //Mitutoyo gauge setup
  Serial.print("No. channels :   "); Serial.println(numCh);
  Serial.print("\t Pinout: (REQ/CLK/DAT)");
  Serial.println();

  for (uint8_t ch = 0; ch<numCh; ch++) {
    pinMode(pinREQ[ch],      OUTPUT);
    pinMode(pinCLK[ch],      INPUT_PULLUP);
    pinMode(pinDAT[ch],      INPUT_PULLUP);

    // set request at high
    digitalWrite(pinREQ[ch], LOW);

    Serial.print("\t ");
    Serial.print(ch);
    Serial.print(" : ");
    Serial.print(pinREQ[ch]);
    Serial.print(", ");
    Serial.print(pinCLK[ch]);
    Serial.print(", ");
    Serial.print(pinDAT[ch]);
    Serial.println();
  }

  
  //Network setup
  Serial.println("Starting networking...");
  #ifdef USE_DHCP
  Ethernet.begin(mac);
  #else
  Ethernet.begin(mac, ip, myDns, gateway, subnet);
  #endif

  // Check for Ethernet hardware present
  if (Ethernet.hardwareStatus() == EthernetNoHardware) {
    Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
    while (true) {
      delay(1); // do nothing, no point running without Ethernet hardware
    }
  }
  while(true)
    if (Ethernet.linkStatus() == LinkOFF) {
      Serial.println("Ethernet cable is not connected :(");
    }
    else {
      Serial.println("Ethernet cable connected / status unknown :)");
      break;
  }
  byte buffMAC[6];
  Serial.print("MAC address:     ");
  Ethernet.MACAddress(buffMAC);
  for (byte octet=0; octet<6;octet++) {
    Serial.print(buffMAC[octet],HEX);
    if(octet < 5) {
      Serial.print(":");
    }
  }
  Serial.println();
  
  Serial.print("IP address:      "); Serial.println(Ethernet.localIP());
  Serial.print("DNS address:     "); Serial.println(Ethernet.dnsServerIP());
  Serial.print("Gateway address: "); Serial.println(Ethernet.gatewayIP());
  Serial.print("Subnet:          "); Serial.println(Ethernet.subnetMask());

  srvWEB.begin();
  for (uint8_t ch = 0; ch<numCh; ch++) {
    srvTXT[ch].begin();
    srvTXT_prevUpdateTime[ch] = 0;
  }
  
  //General
  for (uint8_t ch = 0; ch<numCh; ch++) {
    measData[ch]        = 0.0;
    measDataFresh[ch]   = false;
    measDataIsMm[ch]    = true;
    measDataIsValid[ch] = false;
  }
  for (byte i = 0; i < MAX_SOCK_NUM; i++) {
    clients_ch[i] = 0;
  }

  
  Serial.print("Poll interval  "); Serial.print(update_interval); Serial.println(" [ms]");
  Serial.print("Max. interval  "); Serial.print(srvTXT_max_interval); Serial.println(" [ms]");
  Serial.println();
}










void loop() {
  // ** Flood control **
  //Only update at intervals, not "as fast as we can possibly go"
  //This is the minimum interval; if e.g. web server takes more time, it will be slowed down.
  unsigned long thisUpdateTime = millis();
  if (not (thisUpdateTime - prevUpdateTime >= update_interval)) {
    //Not ready yet.
    //Note that this should be safe when millis rolls over,
    // however the interval when it happens will almost certainly be shorter
    return;
  }
  prevUpdateTime = thisUpdateTime;

  Serial.print("update ");
  Serial.println(prevUpdateTime);

  // ** Handle Mitotoyo instruments **
  for (uint8_t ch = 0; ch<numCh; ch++) {
#ifndef DUMMY_READER //Actually read the Mitutoyo instruments (not DUMMY)
    readSPCinstrument(ch);
#else
    readSPCdummy(ch);
#endif
  }

  // ** Handle network **
  HTTPserver();
  for (uint8_t ch = 0; ch<numCh; ch++) {
    TXTserver(ch);
  }

  // ** Housekeeping   **
  #ifdef USE_DHCP
  DHCPhousekeeping();
  #endif

  for (uint8_t ch = 0; ch<numCh; ch++) {
    measDataFresh[ch] = false;
  }

  Serial.println();
  Serial.println("****************************");
  Serial.println();
}










#ifdef DUMMY_READER
void readSPCdummy(uint8_t ch) {
  //Update this channel?
  if (random(0,100) <= 5 ) { // 5% of the time, update it
    measDataFresh[ch]   = true;
    if (random(0,100) <= 50) { // 50% of the time, the data is invalid
      measDataIsValid[ch] = false;
    }
    else {
      measDataIsValid[ch] = true;
      measData[ch]        = random(-10000,10000)/1000.0; //Between +/- 10 units, 1 um resolution
    }
    Serial.print("Update on ch #");
    Serial.print(ch);
    Serial.print(", new value = ");
    Serial.print(measData[ch],3);
    Serial.print(", IsValid = ");
    Serial.println(measDataIsValid[ch]);
  }
}
#endif










void readSPCinstrument(uint8_t ch) {
    unsigned long SPC_timeout_time = millis()+SPC_timeout;
    
    digitalWrite(pinREQ[ch],HIGH);
    
    byte spcdata[13]; // The raw data sent by instrument
    for(uint8_t i = 0; i < 13; i++ ) {
      uint8_t k = 0;

      // Timing diagram indicates data bit has been valid for about 120 
      // microseconds before the clock signal is raised, and remains
      // valid for about 120 microseconds afterwards. This code reads data
      // bit at the falling clock edge.
      for (uint8_t j = 0; j < 4; j++) {
        while( digitalRead(pinCLK[ch]) == LOW  && millis() < SPC_timeout_time ) { } // hold until clock is high
        while( digitalRead(pinCLK[ch]) == HIGH && millis() < SPC_timeout_time ) { } // hold until clock is low

        if (millis() >= SPC_timeout_time) {
          measDataIsValid[ch] = false;
          measDataFresh[ch]   = true;
          digitalWrite(pinREQ[ch],LOW);

          Serial.print("SPC timeout on ch=");
          Serial.print(ch);
          Serial.print(", i=");
          Serial.print(i);
          Serial.print(", j=");
          Serial.println(j);
          
          return;
        }

        bitWrite(k, j, (digitalRead(pinDAT[ch]) & 0x1));


      }

      // After reading the first 4-bit value, we can drop REQ output.
      if (i == 0) {
        digitalWrite(pinREQ[ch],LOW);
      }
      spcdata[i] = k;
    }

    Serial.print("RAW DATA=");
    for(uint8_t i = 0; i < 13; i++ ) {
      Serial.print(spcdata[i],HEX);
    }

    //Decode
    // Mitutoyo SPC data port transmits 13 4-bit values
    // Byte Meaning
    // 0    Header, always 0xF
    // 1    Header, always 0xF
    // 2    Header, always 0xF
    // 3    Header, always 0xF
    // 4    Sign. Zero is positive. 0x8 is negative.
    // 5    Most significant digit (MSD) of measurement
    // 6    Next digit of measurement
    // 7    Next digit of measurement
    // 8    Next digit of measurement
    // 9    Next digit of measurement
    // 10   Least significant digit (LSD) of measurement
    // 11   Digit to place decimal point
    // 12   Unit. Zero is mm. Nonzero (b1000 is 1?) is inch

    if (not (spcdata[0] == 0xF &&
             spcdata[1] == 0xF &&
             spcdata[2] == 0xF &&
             spcdata[3] == 0xF )) {
      measDataIsValid[ch] = false;
      measDataFresh[ch]   = true;
      Serial.println(" Bad read, descync?");
      return;
    }
    
    float newMeasData = 0.0;
    for(uint8_t i=5; i<=10; i++) {
      newMeasData *= 10;
      newMeasData += spcdata[i];
    }
    uint8_t decimal = spcdata[11];
    newMeasData /= pow(10,decimal);
    if(spcdata[4] == 0x8) {
      newMeasData *= -1;
    }
    bool isMM;
    if(spcdata[12] == 0x0) {
      isMM = true;
    }
    else {
      isMM = false;
    }
   
    Serial.print(" converted=");
    Serial.print(newMeasData,3);
    if (isMM){
      Serial.println("[mm]");
    }
    else{
      Serial.println("[in]");
    }

    if ( newMeasData != measData[ch] || not measDataIsValid[ch] ) {
      measData[ch] = newMeasData;
      measDataIsMm[ch] = isMM;
      measDataFresh[ch] = true;
    }
    measDataIsValid[ch] = true;

    return;

}










void HTTPserver () {
  unsigned long WEB_timeout_time = millis()+WEB_timeout;
  
  EthernetClient client = srvWEB.available();
  if(client) {
    Serial.println();
    Serial.print("New web client ");
    Serial.println(client.remoteIP());
  }
  else {
    return;
  }

  //HTTP Protocol:
  // Client asks for something (i.e. "GET"), then sends a blank line
  // When we (the server) get this blank line, we can send a reply.
  // We should also echo what the client sends
  
  boolean currentLineIsBlank = true;
  while(client.connected() and millis() < WEB_timeout_time ) {
    if (client.available()) {
      //WARNING: If client does not "do it's thing",
      // the Arduino may become unresponsive.
      char c = client.read();
      Serial.write(c);
      if(c == '\n' && currentLineIsBlank) {
        // send a standard http response header
        client.println("HTTP/1.1 200 OK");
        client.println("Content-Type: text/html");
        client.println("Connection: close");  // the connection will be closed after completion of the response
        client.print("Refresh: "); client.println(WEB_update_interval); // refresh the page automatically every WEB_update_interval sec
        client.println();
        client.println("<!DOCTYPE HTML>");
        client.println("<html>");
        
        client.println("<H1>PositionGaugeServer</H1>");
        
        client.println("<p>");
        client.print("IP address:         "); client.print(Ethernet.localIP()); client.println("<br>");
        client.print("No. channels:       "); client.print(numCh); client.println("<br>");
        client.print("Poll interval:      "); client.print(update_interval); client.println(" [ms]<br>");
        client.print("WEB update interval:"); client.print(WEB_update_interval); client.println(" [s]<br>");
        client.print("Max. interval:      "); client.print(srvTXT_max_interval); client.println(" [ms]");
        client.print("Internal time:      "); client.print(millis()); client.println(" [ms]<br>");

        client.println("</p>");

        client.println("<hr>");

        for (uint8_t ch = 0; ch<numCh; ch++) {
          client.print("<h2> Gauge ch #");
          client.print(ch);
          client.println("</h2>");

          client.println("<p>");
          client.print("Value = ");
          client.print(measData[ch],3);
          if (measDataIsMm[ch]) {
            client.print(" mm");
          }
          else {
            client.print(" in");
          }
          if ( measDataIsValid[ch] ) {
            client.print(" VALID");
          }
          else {
            client.print(" INVALID");
          }
          client.println("</p>");

          client.println("<p>");
          client.print("TXTsrv TCP port=");
          client.print(srvTXT_port[ch]);
          client.println("<br>");
          client.print("Connections:<br>");
          for (byte i = 0; i < MAX_SOCK_NUM; i++) {
            if (clients[i] && (clients_ch[i]==ch)) {
              client.print(clients[i].remoteIP());
              client.println("<br>");
            }
          }
          client.println("</p>");
        }

        client.println("<hr>");

        #ifdef DUMMY_READER
        client.println("IN TEST DUMMY MODE");
        client.println("<marquee> IN TEST DUMMY MODE </marquee>");
        client.println("IN TEST DUMMY MODE");
        #endif
        
        client.println("</html>");
        break;
      }
      if(c == '\n') {
        //Starting a new line
        currentLineIsBlank = true;
      }
      else if (c != '\r') {
        //Got a charracter on current line
        currentLineIsBlank = false;
      }
      
    }
  }
  
  if ( millis() >= WEB_timeout_time) {
    Serial.println();
    Serial.println("WEB timed out.");
  }
  
  // give the web browser time to receive the data
  delay(10);//[ms]
  // close the connection:
  client.stop();
  Serial.println("web client disconnected");
}










void TXTserver (uint8_t ch) {
  //1. Check for new connections
  while(true) {
    //Loop in case there are multiple new connections
    EthernetClient newClient = srvTXT[ch].accept();
    if(newClient) {
      //find a new 'client' slot
      for (byte i = 0; i < MAX_SOCK_NUM; i++) {
        if(!clients[i]) {
          clients[i] = newClient;
          clients_ch[i] = ch;
          
          //clients[i].print("CH=");
          //clients[i].println(ch);
  
          //Feed the new client some data right away
          if(measData[ch] >= 0) {
            //Add the '+' so that the string is always the same length
            clients[i].print('+');
          }
          clients[i].print(measData[ch],3);
          if (measDataIsValid[ch]) {
            if (measDataIsMm[ch]) {
              clients[i].println("mm");
            }
            else {
              clients[i].println("in");
            }
          }
          else {
            clients[i].println("XX");
          }
  
          Serial.print("New connection for ch=");
          Serial.print(ch);
          Serial.print(" to slot ");
          Serial.println(i);
          
          break;
        }
      }
    }
    else {
      break;
    }
  }

  //2. Check for incoming data and discard it
  for (byte i = 0; i < MAX_SOCK_NUM; i++) { 
    while (clients[i] && (clients_ch[i]==ch) && clients[i].available() > 0) {
      clients[i].read();
    }
  }
  
  //3. If fresh data is available or too long since last update, send it!
  unsigned long timeSinceLastUpdate = millis() - srvTXT_prevUpdateTime[ch];
  if (measDataFresh[ch] or (timeSinceLastUpdate >= srvTXT_max_interval)) {
    srvTXT_prevUpdateTime[ch] = millis();
    for (byte i = 0; i < MAX_SOCK_NUM; i++) {
      if (clients[i] && (clients_ch[i]==ch)) {
        if(measData[ch] >= 0) {
          //Add the '+' so that the string is always the same length
          clients[i].print('+');
        }
        clients[i].print(measData[ch],3);
        if (measDataIsValid[ch]) {
          if (measDataIsMm[ch]) {
            clients[i].println("mm");
          }
          else {
            clients[i].println("in");
          }
        }
        else {
          clients[i].println("XX");
        }
      }
    }
  }

  //4. Check for disconnects
  for (byte i = 0; i < MAX_SOCK_NUM; i++) { 
    if ( clients[i] && (clients_ch[i]==ch) && !clients[i].connected() ) {
      clients[i].stop();

      Serial.print("Closed connection for ch=");
      Serial.print(ch);
      Serial.print(" slot ");
      Serial.println(i);
    }
  }
}










#ifdef USE_DHCP
void DHCPhousekeeping() {
  switch (Ethernet.maintain()) {
    case 1:
      //renewed fail
      Serial.println("Error: renewed fail");
      break;

    case 2:
      //renewed success
      Serial.println("Renewed success");
      //print your local IP address:
      Serial.print("My IP address: ");
      Serial.println(Ethernet.localIP());
      break;

    case 3:
      //rebind fail
      Serial.println("Error: rebind fail");
      break;

    case 4:
      //rebind success
      Serial.println("Rebind success");
      //print your local IP address:
      Serial.print("My IP address: ");
      Serial.println(Ethernet.localIP());
      break;

    default:
      //nothing happened
      break;
  }
}
#endif

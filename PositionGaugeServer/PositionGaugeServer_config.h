#ifndef __PositionGaugeServer_config_h__
#define __PositionGaugeServer_config_h__

// ***** Mitutoyo instrument configuration *****

//Uncomment this to enable the "dummy reader" code instead of the SPC protocol interface
// This disables the actual reading of the instrument,
// however pins are still configured on start
// This enables debugging of the network code if no Mitutoyo instrument is connected.
//#define DUMMY_READER

const uint8_t numCh = 2; //How many instruments connected
                         // Instruments are configured by arrays
                         // pinREQ / pinDAT / pinCLK / srvTXT_port / srvTXT
                         // Remember to update if changing the config!
//Arduino pins
// Used for Ethernet shield -- AVOID:
// 4/10/11/12/13 (UNO)
// 50/51/52/10/4; 53 unused but MUST be output (Mega)
const uint8_t pinREQ[numCh] = {5,7};
const uint8_t pinDAT[numCh] = {2,8};
const uint8_t pinCLK[numCh] = {3,9};

// ***** NETWORK CONFIG ************************
// MAC address for the Arduino on the plasma lens
//byte mac[] = {0xA8, 0x61, 0x0A, 0xAE, 0x84, 0xED};

// MAC address for the Arduino on the CLIC structure
//byte mac[] = {0xA8, 0x61, 0x0A, 0xAE, 0x84, 0xEE};

// MAC address for Kyrre's test board
byte mac[] = {0xA8, 0x61, 0x0A, 0xAE, 0x72, 0xF7};

//To use DHCP or not to use DHCP, that's the question.
// Comment out to use static IP configuration
#define USE_DHCP

#ifndef USE_DHCP
IPAddress ip(192, 168, 1, 177);
IPAddress myDns(192, 168, 1, 1);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 0, 0);
#endif

// ***** OTHER CONFIG ***************************
const unsigned long update_interval     = 1000; //[ms] How often to loop
                                                // (poll sensors, check for new network clients,
                                                //  feed data to network clients, DHCP housekeeping)
const unsigned long WEB_update_interval = 5;    //[s]  WEB requests are heavy, not too often is good.
const unsigned long SPC_timeout         = 200;  //[ms] Experimentally verified to be OK.
const unsigned long WEB_timeout         = 700;  //[ms] Max time allowed waiting for web client

const unsigned long srvTXT_max_interval = 10000;//[ms] At least try to give an update this often


#endif

require stream
#require iocstats

epicsEnvSet("TOP" "$(E3_CMD_TOP)")
epicsEnvSet("STREAM_PROTOCOL_PATH", ".:$(TOP)/db")

#CONNECTION CONFIGURATION
epicsEnvSet("IOCNAME", "TESTGAUGES")
epicsEnvSet("ARDUINO_IP", "192.168.1.31")
epicsEnvSet("PORT_X", "21")
epicsEnvSet("PORT_Y", "22")

drvAsynIPPortConfigure("CONN_X", "$(ARDUINO_IP):$(PORT_X)", 0, 0, 0)
drvAsynIPPortConfigure("CONN_Y", "$(ARDUINO_IP):$(PORT_Y)", 0, 0, 0)

dbLoadRecords("$(TOP)/db/positionGauge.db", "SYSDEV=$(IOCNAME):X,PORT=CONN_X")
dbLoadRecords("$(TOP)/db/positionGauge.db", "SYSDEV=$(IOCNAME):Y,PORT=CONN_Y")

dbLoadRecords("iocAdminSoft.db","IOC=$(IOCNAME):IocStat")

iocInit()

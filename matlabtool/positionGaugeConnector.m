% Kyrre Sjobak, May 2021
% For newest version and example for how to use, please see
%   https://gitlab.cern.ch/CLEAR/arduino-positiongaugeserver/
%   Subfolder 'matlabtool'


classdef positionGaugeConnector < handle
    %POSITIONGAUGECONNECTOR Class for connecting to a channel from
    %   a positionGauge Arduino. See testClass.m for example of how to use
    %   it.
    
    properties (SetAccess = protected)
        lastPos   % Last valid position read from the instrument,
                  %  or NaN if no (valid) data was recieved.
        lastUnit  % Last valid unit read from the instrument ('mm' or 'in'),
                  %  or 'XX' if no (valid) data was recieved.
        lastTime  % Time of last reading, or start of connecting.
                  %  Note, time of reading may be (much) later than time of sending.
        
        lastMessageTime;  % Time of last message reading, valid or not. If valid, is equal to lastTime.
        lastMessageValid; % True if the last recieved message was valid, false if not.
        
        lastReadNumData;  % Amount of data read in the last call to CheckDatastream()
    end
    
    properties (SetAccess = immutable)
        address % Hostname or IP of Arduino, as a string.
        TCPport % TCP port of channel, as an int
    end
    
    properties (Access = private)
        TCPconn % TCP connection object.
                % Connection is automatically closed when
                % postionGaugeConnector is destroyed.
        buff    % Internal data buffer
    end
    
    methods
        function obj = positionGaugeConnector(address,TCPport)
            %POSITIONGAUGECONNECTOR Construct an instance of this class
            %   It needs the hostname and a TCP port to connect
            %   Typically, x-axis is port 21, y-axis port 22.
            
            obj.address = address;
            obj.TCPport = TCPport;
            
            disp(['Connecting to ', obj.address, ' on port ', num2str(obj.TCPport), '...']);
            obj.TCPconn = tcpclient(obj.address,obj.TCPport);
            disp(['Connected!'])
            obj.buff = '';
            
            obj.lastPos = NaN;
            obj.lastUnit = 'XX';
            obj.lastTime = clock();
            obj.lastMessageTime = obj.lastTime;
            obj.lastMessageValid = false;
            obj.lastReadNumData = 0;
            
            %Wait for the Arduino to send the 'welcome' message
            pause(1.0);
            %Then read the welcome message, containing data
            % (avoid almost guaranteed NaN value, arduino is slow)
            CheckDatastream(obj);
        end
        
        function delete(obj)
            %DELETE Closes the TCP connection
            disp(['Disconnecting from ', obj.address, ' on port ', num2str(obj.TCPport), '...']);
            clear obj.TCPconn;
        end
        
        function [pos,unit,time] = getLastValidPos(obj)
            %GETLASTVALIDPOS Reads from device and
            %   returns the latest valid data

            CheckDatastream(obj);

            pos = obj.lastPos;
            unit = obj.lastUnit;
            time = obj.lastTime;
        end

        function CheckDatastream(obj)
            %CHECKDATASTREAM Reads from device
            %   and updates lastPos,lastUnit,lastTime
            %   as well as lastMessageTime and lastMessageValid
            
            %Get the data
            n = obj.TCPconn.BytesAvailable;
            if n > 0
                dat=(obj.TCPconn.read());
                obj.buff = [obj.buff,dat];
            end
            
            %Newlines -> New data!
            k = regexp(obj.buff,'\r\n');
            ik0 = 1;
            nums = [];
            units = {};
            for ik = 1:length(k)
                buff2 = obj.buff(ik0:k(ik)-1);
                
                nums  = [nums, str2double(buff2(1:6))]; %#ok<AGROW>
                units{length(units)+1} = buff2(7:8); %#ok<AGROW>
                
                obj.lastMessageTime = clock();
                if (strcmp(units{length(units)}, 'mm') || strcmp(units{length(units)}, 'in') )
                    obj.lastPos  = nums(length(nums));
                    obj.lastUnit = units{length(units)};
                    obj.lastTime = obj.lastMessageTime;
                    obj.lastMessageValid = true;
                else
                    if(strcmp(units{length(units)}, 'XX')==0)
                        warning(['Found unit=''', units{length(units)}, ''''])
                    end
                    obj.lastMessageValid=false;
                end
                
                ik0=k(ik)+2;
            end
            obj.lastReadNumData = length(k);
            
            obj.buff=obj.buff(ik0:end);
            
        end
    end
end


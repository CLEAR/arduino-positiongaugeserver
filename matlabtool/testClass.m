
hostname = '192.168.1.24';

%Note: Disconnection happens after connection.
% This is OK (will open 2 connections per port for a short while).
xconn = positionGaugeConnector(hostname,21);
yconn = positionGaugeConnector(hostname,22);

%Hit Control+C to cancel
for i=1:10000
    disp(num2str(i));
    
    [xpos,xunit,xtime] = xconn.getLastValidPos();
    [ypos,yunit,ytime] = yconn.getLastValidPos();
    
    disp([' X ', num2str(xpos,'%+6.3f'), ' ', xunit, ' ', num2str(xconn.lastReadNumData), ' ', ' [', num2str(xtime), ']']);
    disp([' Y ', num2str(ypos,'%+6.3f'), ' ', yunit, ' ', num2str(yconn.lastReadNumData), ' ', ' [', num2str(ytime), ']']);
    
    pauseTime = floor(log(i));
    disp(['Pause for ', num2str(pauseTime), 's']);
    disp('');
    pause(pauseTime);
end
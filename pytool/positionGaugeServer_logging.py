#! /usr/bin/env python3

#HOW TO USE THIS SCRIPT:
# run "python3 positionGaugeServer_logging.py ARDUINO_HOSTNAME_OR_IP | tee LOGFILE.TXT"
# This will connect to the given arduino,
# and continiously write a file "LOGFILE.TXT" (pick a sensible name!).
# Due to the use of "tee" it will also show the contents of this file while it is running.
# To stop running, hit Control+C

import sys

if not len(sys.argv) == 2:
    print ("Usage: ./positionGaugeServer_logging.py Arduino_hostname_or_IP")
    exit(1);
IP = sys.argv[1]

import socket

BUFFER_SIZE = 128

portX = 21
portY = 22

sockX = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sockX.connect((IP,portX))
sockX.setblocking(False)
Xdata = None
Xbuff = b""

sockY = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sockY.connect((IP,portY))
sockY.setblocking(False)
Ydata = None
Ybuff = b""

import datetime
import time
t0 = None
XdataPrev = None;
YdataPrev = None;

#We will just assume that unit is in mm
print ("#     Date            Time X[mm] Y[mm]", flush=True)

#Loop to recieve data
while True:
    try:
        Xbuff += sockX.recv(BUFFER_SIZE)
    except:
        pass
    #print("X:", Xbuff)
    if len(Xbuff) > 0 and Xbuff[-1] == b'\n'[0]:
        if len(Xbuff) > 10:
            #In the beginning we get two lines in a hurry.
            #Skip'em. (but using them for XdataPrev would be better)
            #Note that it can contain a "-"!
            Xbuff = b""
        else:
            #Strip off the \r\n and the unit
            Xdata = str(Xbuff[:-4],'ascii')
            Xbuff = b""

    try:
        Ybuff += sockY.recv(BUFFER_SIZE)
    except:
        pass
    #print("Y:", Ybuff)
    if len(Ybuff) > 0 and Ybuff[-1] == b'\n'[0]:
        if len(Ybuff) > 10:
            Ybuff = b""
        else:
            Ydata = str(Ybuff[:-4],'ascii')
            Ybuff = b""

    #print(Xdata, Ydata)

    if (Xdata != None) and (Ydata != None):
        print(datetime.datetime.now(), Xdata, Ydata, flush=True)
        XdataPrev = Xdata
        Xdata = None
        YdataPrev = Ydata
        Ydata = None
    #If we had an update from one gauge only, max wait 1 sec to show it
    elif Xdata != None:
        if t0 == None:
            t0 = time.time()
        elif (time.time() - t0) > 1:
            t0 = None
            print(datetime.datetime.now(), Xdata, YdataPrev, flush=True)
            XdataPrev = Xdata
            Xdata = None
    elif Ydata != None:
        if t0 == None:
            t0 = time.time()
        elif (time.time() - t0) > 1:
            t0 = None
            print(datetime.datetime.now(), XdataPrev, Ydata, flush=True)
            YdataPrev = Ydata
            Ydata = None
    time.sleep(1.0)
sockX.close()
sockY.close()

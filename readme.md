# Arduino Position gauge server

By K. Sjobak, A. Gilardi, P. Korysko, and W. Farabolini

This code allows connecting multiple Mitutoyo position gauges to an Ethernet IP network, and transmit new readings to the connected clients.
The transmission is done over one TCP channel pr. gauge, per default running on port 21 and 22.
Additionally, a simple web/HTTP server is provided in order to check the status of the system.

To connect to standard readout method, use netcat as follows (assuming the IP address is `192.168.1.31`; hostname can also be used here):
* `nc 192.168.1.31 21` For the horizontal channel
* `nc 192.168.1.31 22` For the vertical channel

To connect to web interface, point your web browser at (note http, not https):
* `http://192.168.1.31/`

The code logic and global variable definition is contained in the file `PositionGaugeServer.ino`.
The configuration which must be changed by the user needs for each deployment is contained in the header file `PostionGaugeServer_config.h`, both in the `PositionGaugeServer` folder, and includes:
* Ethernet MAC address (**must** be set differently for each deployment)
* DHCP or fixed IP configuration
* Refresh intervals
* Timeouts
* Number of gauges to connect
* Pins to use for communication with gauges

The project only depends on the standard Arduino libraries and the Ethernet library in order to support the Ethernet shield .
It is tested and deployed with an Arduino Uno and a Ethernet Shield v2, connected to two gauges, as shown in the picture below.

The schematic for the connection to the gauge (provided by Roger Cheng) looks like:
![Simple schematic](/schematic.png)
Note that this is for a single gauge, and the pins on the Arduino may have changed.

The actual deployed hardware components look like, including 3D printed case, Arduino Uno Microcontroller, Ethernet shield, and special adapter board for gauges:
![All componets layed out](/AllComponents.jpg)
The special adapter board, including a small bodge wire in order to avoid a pin that is used by the Ethernet shield:
![Adapter board, top](/AdapterBoardTop.jpg)
![Adapter board, bottom](/AdapterBoardBot.jpg)

Deployment example, in order to monitor the position of the CLIC structure mover at CLEAR during operation of the accelerator:
![Gauges on CLIC structure](CLIC_structure_mitutoyos.jpg)

Example of the web page served by the Arduino:
![Screenshot of web browser](/web_mitutoyo_clic_structure.png)

A presentation of the current deployment is shown at: http://pkorysko.web.cern.ch/Arduinos_and_Mitutoyos.html

Big thanks to Roger Cheng, who's code [1] inspired this code, as well as an instructables project [2] that provided inspiration for the hardware design.

## References

 [1] https://github.com/Roger-random/mitutoyo
 
 [2] https://www.instructables.com/Interfacing-a-Digital-Micrometer-to-a-Microcontrol/